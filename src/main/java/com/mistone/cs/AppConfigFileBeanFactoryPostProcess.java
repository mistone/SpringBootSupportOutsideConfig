package com.mistone.cs;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.io.ResourceLoader;

/**
 * 注册应用自定义配置文件路径解析
 *
 * @author mistone
 * @version 1.0
 * @created
 **/

public class AppConfigFileBeanFactoryPostProcess implements BeanDefinitionRegistryPostProcessor, PriorityOrdered {
    private AbstractApplicationContext context;

    public AppConfigFileBeanFactoryPostProcess(ConfigurableApplicationContext context) {
        if(context instanceof AbstractApplicationContext){
            this.context = (AbstractApplicationContext) context;
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        if(registry instanceof  ConfigurableListableBeanFactory){
            ConfigurableListableBeanFactory beanFactory = (ConfigurableListableBeanFactory) registry;
            AppConfigFileBeanPostProcess acf = new AppConfigFileBeanPostProcess(context);
            beanFactory.addBeanPostProcessor(acf);
            beanFactory.registerResolvableDependency(ResourceLoader.class, acf);
        }
    }
}
