package com.mistone.cs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述
 *
 * @author mistone
 * @version 1.0
 * @created
 **/
@RestController
public class CsController {
    @Autowired
    private CsProperties cs;
    @RequestMapping("/")
    public String index(){
        return cs.getName()+"-"+cs.getPassword();
    }
}
