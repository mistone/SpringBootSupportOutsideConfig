package com.mistone.cs;

import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 注册应用自定义配置文件路径解析
 *
 * @author mistone
 * @version 1.0
 * @created
 **/

public class AppConfigFileApplicationListener implements ApplicationListener {
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ApplicationPreparedEvent) {
            onApplicationPreparedEvent(event);
        }
    }

    private void onApplicationPreparedEvent(ApplicationEvent event) {
        addPostProcessors(((ApplicationPreparedEvent) event).getApplicationContext());
    }

    protected void addPostProcessors(ConfigurableApplicationContext context) {
        context.addBeanFactoryPostProcessor(new AppConfigFileBeanFactoryPostProcess(context));
    }

}
