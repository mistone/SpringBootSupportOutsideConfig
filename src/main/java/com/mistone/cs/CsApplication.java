package com.mistone.cs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 描述
 *
 * @author mistone
 * @version 1.0
 * @created
 **/
@SpringBootApplication
public class CsApplication {
    public static void main(String[] args) {
        SpringApplication.run(CsApplication.class,args);
    }
}
