package com.mistone.cs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 描述
 *
 * @author mistone
 * @version 1.0
 * @created
 **/
@Component
@PropertySource(value="cs.properties")
@ConfigurationProperties(prefix = "hh")
@Data
public class CsProperties {
    private String name;
    private String password;
}
